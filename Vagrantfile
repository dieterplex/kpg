# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial32"
  config.vm.box_check_update = false
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.synced_folder "./data", "/data"
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "2048"
    vb.customize ["modifyvm", :id, "--accelerate3d", vb.gui ? "on" : "off"]
    vb.customize ["modifyvm", :id, "--clipboard", vb.gui ? "bidirectional" : "disabled"]
    vb.customize ["modifyvm", :id, "--vram", vb.gui ? "256" : "12"]
  end

  config.vm.provision "bootstrap", type: "shell", inline: <<-SHELL.gsub(/^ +/, '')
    export DEBIAN_FRONTEND="noninteractive"
    debconf-set-selections <<< "nodm nodm/enabled boolean true"
    debconf-set-selections <<< "nodm nodm/user string root"
    apt -qq update
    apt install -qy gcc make gawk libtool vim ranger rxvt-unicode i3 nodm xserver-xorg xserver-xephyr fonts-noto-mono
    cp /vagrant/.Xdefaults /root/
    cat ~vagrant/.ssh/authorized_keys > /root/.ssh/authorized_keys
    echo '"mod + enter" to open terminal and "mod + d" to run command. see ~/.config/i3/config'
  SHELL

  config.vm.provision "inst_kitty", type: "shell", run: "never", inline: <<-SHELL.gsub(/^ +/, '')
    apt install -qy gcc g++ autoconf unzip python3-pygments imagemagick
    apt install -qy libcairo2-dev libfreetype6-dev libglib2.0-dev libgl1-mesa-dev \
                    libxcursor-dev libxi-dev libxinerama-dev libxkbcommon-x11-dev libxkbcommon-dev libxrandr-dev \
                    libdbus-1-dev libffi-dev libxml2-dev python3-dev

    ## build harfbuzz
    cd /tmp
    wget http://archive.ubuntu.com/ubuntu/pool/main/h/harfbuzz/harfbuzz_1.8.8.orig.tar.bz2
    tar xf harfbuzz_1.8.8.orig.tar.bz2
    cd harfbuzz-1.8.8/ && ./configure && make && make install

    ## install local libpng16 without break dependency
    cd /tmp
    wget http://archive.ubuntu.com/ubuntu/pool/main/libp/libpng1.6/libpng1.6_1.6.34.orig.tar.xz
    tar xf libpng1.6_1.6.34.orig.tar.xz
    cd libpng-1.6.34/ && ./configure && make && make install

    ## install wayland & wayland-protocols
    cd /tmp
    wget http://archive.ubuntu.com/ubuntu/pool/main/w/wayland/wayland_1.16.0-1ubuntu1.tar.gz
    tar xf wayland_1.16.0-1ubuntu1.tar.gz
    cd wayland.git/ && autoreconf --force -v --install && ./configure --disable-documentation && make && make install
    cd /tmp
    wget http://archive.ubuntu.com/ubuntu/pool/main/w/wayland-protocols/wayland-protocols_1.16.orig.tar.xz
    tar xf wayland-protocols_1.16.orig.tar.xz
    cd wayland-protocols-1.16/ && ./configure && make && make install
    
    ## build kitty
    cd /data
    wget https://github.com/kovidgoyal/kitty/archive/master.tar.gz
    tar xf master.tar.gz
    cd kitty-master
    sed -i 's/^\\(\\s\\+copy_man_pages\\)/\#\\1/' setup.py
    sed -i 's/^\\(\\s\\+copy_html_docs\\)/\#\\1/' setup.py
    sed -i 's/^\\(\\s\\+\\)\\(run_tool.*docs\\)/\\1pass\# \\2/' setup.py
    LDFLAGS='-Wl,-rpath -Wl,/usr/local/lib -L/usr/local/lib' python3 setup.py linux-package
    cp -r linux-package/* /usr/local/

    ## Add i3 shortcut for kitty
    echo 'bindsym $mod+Shift+Return exec kitty' >> ~/.config/i3/config
  SHELL

end
